// console.log("Hello World")

// [SECTION] Objects
/*
	Syntax:
		let/const objectName = {
			keyA: valueA,
			keyB: valueB
		}
*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
}

console.log("Result from creating objects using initiliazers/literal notation: ")
console.log(cellphone)
console.log(typeof cellphone)

// this is an object
function Laptop(name, manufactureDate) {
	this.name = name 
	this.manufactureDate = manufactureDate
}

// this is an instance of an object
let laptop = new Laptop("Lenovo", 2008)
console.log("Result from creating objects using object constructors: ")
console.log(laptop)

let myLaptop = new Laptop("Macbook Air", 2020)
console.log("Result from creating objects using object constructors: ")
console.log(myLaptop)

// we cannot create new objects without instance or the "new" keyword.
let oldLaptop = /* new */ Laptop("Portal R2E CCMC", 1980)
console.log("Result from creating objects using object constructors: ")
console.log(oldLaptop) // returns undefined

// creating empty objects
let computer = {}
console.log(computer)

// accessing objects in an array
let array = [laptop, myLaptop]

console.log(array[0]["name"])
console.log(array[0].name)

// we can create objects with:

// {} - object literal -> great for creating dynamic objects
let pokemon1 = {
	name: "Pikachu",
	type: "Electric"
}

let pokemon10 = {
	name: "Mew",
	type: ["Psychic", "Normal"]
}

let pokemon25 = {
	name: "Charizard",
	level: 30
}

// Constructor Function - allows us to create objects with a defined structure
function Pokemon(name, type, level) {
	// "this" keyword is used to refer to the object to be created with the constructor function
	this.name = name
	this.type = type
	this.level = level
}

// "new" keyword allows us to create an instance of an object with a constructor function
let pokemonInstance = new Pokemon("Balbasaur", "Grass", 32)

// objects can have arrays as properties:

let professor1 = {
	name: "Romenick Garcia",
	age: 25,
	subjects: ["MongoDB", "HTML", "CSS", "JS"]
}

// we can access object properties with dot notation
console.log(professor1.subjects)

// we can add items into the array by accesing the key array and use push()
professor1.subjects.push("NodeJS")
console.log(professor1.subjects)

// [Mini Activity] Display in the console each subject from professor1' subjects array

// Solution # 1 - we can also use for() loop to display each item in the array:
for(let i = 0; i < professor1.subjects.length; i++) {
	console.log(professor1.subjects[i])
}

// Solution # 2 - we can also use forEach() to display each item in the array:
professor1.subjects.forEach(function(subject) {
	console.log(subject)
})

// Solution # 3 - access the items from the array using its index:
console.log(professor1.subjects[0])
console.log(professor1.subjects[1])
console.log(professor1.subjects[2])
console.log(professor1.subjects[3])
console.log(professor1.subjects[4])

// Array of objects:

let dog1 = {
	name: "Bantay",
	breed: "Golden Retriever"
}

let dog2 = {
	name: "Bolt",
	breed: "Aspin"
}

let dogs = [dog1, dog2]

// you can use the index number of the item to access it from the array
console.log(dogs[0])

console.log(dogs[1].name)
console.log(dogs[1]["name"])

// delete an object inside an array
dogs.pop()
console.log(dogs)

// add objects inside an array
dogs.push({
	name: "Whitey",
	breed: "Shih Tzu"
})

// [SECTION] Initializing, Deleting and Re-assigning Object Properties

let supercar1 = {}
console.log(supercar1)

// initialize properties and values with our empty object using key-value pairs
supercar1.brand = "Porsche"
console.log(supercar1)

supercar1.model = "Porsche 911"
supercar1.price = 182900
console.log(supercar1)

// delete object properties with the "delete" keyword
delete supercar1.price
console.log(supercar1)

// update the values of an object using the dot notation
supercar1.model = "718 Boxster"
console.log(supercar1)

pokemonInstance.type = "Grass, Normal"
console.log(pokemonInstance)

// [SECTION] Object Methods
// functions in an object

let person1 = {
	name: "Joji",
	talk: function() {
		console.log("Hello!")
	}
}

person1.talk();
// talk() -> will now work as the talk() is method for the object only

let person2 = {
	name: "Joe",
	talk: function() {
		console.log("Hello World!")
	}
}

person2.talk();

person2.walk = function() {
	console.log("Joe has walked 500 miles just to be the man that walked 1000 miles to be at your door.")
}

// walk() -> error
// person1.walk() -> error
person2.walk()

// "this" keyword is an object method
person1.introduction = function() {
	console.log("Hi! I am " + this.name + "!")
}

person1.introduction()

person2.introduction = function() {
	console.log(this)
}

person2.introduction()

// [Mini Activity]
let student1 = {
	name: "Hillary Mae Almonte",
	age: 22,
	address: "Caloocan City"
}

student1.introduceName = function() {
	console.log("Hello! My name is " + this.name)
}

student1.introduceAge = function() {
	console.log("Hello! I am " + this.age + " years old.")
}

student1.introduceAddress = function() {
	console.log("Hello! I am " + this.name + ". I live in " + this.address)
}

student1.introduceName()
student1.introduceAge()
student1.introduceAddress()

function Student(name, age, address) {
	this.name = name
	this.age = age
	this.address = address

	this.introduceName = function() {
	console.log("Hello! My name is " + this.name)
	}

	this.introduceAge = function() {
	console.log("Hello! I am " + this.age + " years old.")
	}

	this.introduceAddress = function() {
	console.log("Hello! I am " + this.name + ". I live in " + this.address)
	}

	// we can also add methods that take other objects as an argument 
	this.greet = function(person) {
		// person's properties are accessible in the method
		console.log("Good Day, " + person.name + "!")
	}
}

let newStudent1 = new Student("Hillary Mae Almonte", 22, "Caloocan City")
console.log(newStudent1)

let newStudent2 = new Student("Jeremiah", 26, "Lucban Quezon")
newStudent2.greet(newStudent1)

// [Mini Activity]

function Dog(name, breed) {
	this.name = name
	this.breed = breed

	this.call = function() {
		console.log("Bark Bark Bark!")
	}

	this.greet = function(person) {

		// hasOwnProperty() method will check if the object passed has the indicated property
		// console.log(person.hasOwnProperty("name"))
		
		// console.log("Bark Bark," + person.name)
	}
}

let talkingdog1 = new Dog("Jotaro", "Dachshund")
talkingdog1.call()
talkingdog1.greet(newStudent1)
talkingdog1.greet(supercar1)

function sample1(sampleParameter) {
	console.log(sampleParameter.name)
}

sample1(talkingdog1)
sample1(25000)